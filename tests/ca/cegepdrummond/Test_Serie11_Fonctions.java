package ca.cegepdrummond;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class Test_Serie11_Fonctions extends SimulConsole {
    @Test
    @Order(1)
    void test_fonction1() throws Exception {
        choixMenu("11a");
        ecrire("12");
        assertSortieContient("-11.111");

        choixMenu(".");
        ecrire("-40");
        assertSortieContient("-40");

        choixMenu(".");
        ecrire("32");
        assertSortieContient("0.0");
    }
    @Test
    @Order(2)
    void test_fonction2() throws Exception {
        choixMenu("11b");
        ecrire("12");
        assertSortieContient("-11.111");

        choixMenu(".");
        ecrire("-40");
        assertSortieContient("-40");

        choixMenu(".");
        ecrire("32");
        assertSortie("0.0", false);
    }

    @Test
    @Order(3)
    void test_fonction3() throws Exception {
        choixMenu("11c");
        ecrire("3");
        assertSortie("0 1 1", false);

        choixMenu(".");
        ecrire("5");
        assertSortie("0 1 1 2 3", false);
    }

    @Test
    @Order(4)
    void test_fonction4() throws Exception {
        choixMenu("11d");
        ecrire("3");
        assertSortie("0 1 1", false);

        choixMenu(".");
        ecrire("5");
        assertSortie("0 1 1 2 3", false);
    }

    @Test
    @Order(5)
    void test_fonction5() throws Exception {
        choixMenu("11e");
        ecrire("5 5");
        assertSortie("25", false);

        choixMenu(".");
        ecrire("20 10");
        assertSortie("200", false);

    }

    @Test
    @Order(6)
    void test_fonction6() throws Exception {
        choixMenu("11f");
        ecrire("1 2 4 6");
        assertSortie("5.0", false);

        choixMenu(".");
        ecrire("1 1 3 1");
        assertSortie("2.0", false);
    }

    @Test
    @Order(7)
    void test_fonction7() throws Exception {
        choixMenu("11g");
        ecrire("0");
        assertSortie("false", false);

        choixMenu(".");
        ecrire("1");
        assertSortie("false", false);

        choixMenu("11g");
        ecrire("2");
        assertSortie("true", false);

        choixMenu(".");
        ecrire("3");
        assertSortie("true", false);

        choixMenu(".");
        ecrire("21");
        assertSortie("false", false);

        choixMenu(".");
        ecrire("31");
        assertSortie("true", false);

    }

    @Test
    @Order(8)
    void test_fonction8() throws Exception {
        choixMenu("11h");
        ecrire("1");
        assertSortie("1", false);

        choixMenu(".");
        ecrire("2");
        assertSortie("2", false);

        choixMenu(".");
        ecrire("3");
        assertSortie("6", false);

        choixMenu(".");
        ecrire("5");
        assertSortie("120", false);


    }

    @Test
    @Order(9)
    void test_fonction9() throws Exception {
        choixMenu("11i");
        ecrire("3");
        assertSortie("^", false);
        assertSortie("^^", false);
        assertSortie("^^^", false);

        choixMenu(".");
        ecrire("5");
        assertSortie("^", false);
        assertSortie("^^", false);
        assertSortie("^^^", false);
        assertSortie("^^^^", false);
        assertSortie("^^^^^", false);



    }

}
