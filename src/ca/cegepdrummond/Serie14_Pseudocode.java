package ca.cegepdrummond;

import java.util.Scanner;

public class Serie14_Pseudocode {
    /*
     * Vous devez créer un programme correspondant au pseudocode suivant:
     *
     * Lire 3 ensembles de coordonnées (x1, y1) (x2, y2) (x3, y3) représentants 3 points.
     * Trouver les 2 points les plus distants. Nous appelerons "Hypothénuse" la distance entre ces 2 points )
     * (indice: vous avez fait un exercice pour calculer la distance entre 2 points
     *          dans la section des fonctions (fonction6) )
     *
     * Les deux autres distances seront appelées "A" et "B"
     *
     *
     * Vérifier si
     *       A^2 + B^2 = Hypothénuse^2    >>> ^2 veut dire "au carré"
     *
     *
     * si oui, afficher "carré"
     * si non, afficher "quelconque"
     *
     * voici des valeurs à essayer:
     *
     * 3 0
     * 0 4
     * 0 0
     * devrait afficher "carré"
     *
     * 3 0
     * 0 4
     * 1 1
     * devrait afficher "quelconque"
     *
     * 11 0
     * 0 60
     * 0 0
     * devrait afficher "carré"
     *
     *
     * Pouvez-vous expliquer ce que détermine ce programme ?
     *
     * Explication des tests automatisés
     * Les tests automatisés vont aussi essayer les valeurs suivantes
     * 10 0
     * 0 10
     * 0 0
     *
     * Si vous faites les calculs à la main, vous devriez obtenir "carré".
     * Mais il y a de fortes chances que vous ayez obtenue une erreur durant les tests automatisés.
     *
     * Je vous expliquerai les erreurs d'arrondissements un peu plus tard.
     * Vous pouvez essayer de trouver ou cette erreur se produit, et comment la compenser.
     *
     *
     */
    public void pseudo1() {
        Scanner s = new Scanner(System.in);
        double hypothenuse, A, B;

        double x1 = s.nextDouble();
        double y1 = s.nextDouble();
        double x2 = s.nextDouble();
        double y2 = s.nextDouble();
        double x3 = s.nextDouble();
        double y3 = s.nextDouble();


        double longueur1_2 = distance(x1,y1,x2,y2);
        double longueur2_3 = distance(x2,y2,x3,y3);
        double longueur3_1 = distance(x3,y3,x1,y1);

        if(longueur1_2 > longueur2_3 && longueur1_2 > longueur3_1){
            hypothenuse = longueur1_2;
            A = longueur2_3;
            B = longueur3_1;
         }else if(longueur2_3 > longueur1_2 && longueur2_3 > longueur3_1){
            hypothenuse = longueur2_3;
            A = longueur1_2;
            B = longueur3_1;
         }else{
            hypothenuse = longueur3_1;
            A = longueur1_2;
            B = longueur2_3;
        }

        boolean reponse = verifie(hypothenuse,A,B);

        if(reponse == true){
            System.out.println("carré");
        }else{
            System.out.println("quelconque");
        }
        // verifier si c'est carré ou quelconque.
        // indice: la fonction "verifie" est à terminer.




    }


    /**
    * Calcule la distance entre 2 points.
    * @param x1
    * @param y1
    * @param x2
    * @param y2
    * @return la distance entre les deux points (x1, y1) et (x2, y2)
    */

    public static double distance(double x1, double y1, double x2, double y2) {
        return Math.sqrt(Math.pow(x2-x1,2) + Math.pow(y2-y1,2));
    }

    /**
     * Vérifie si les trois coordonées donne un triangle rectangle.
     * @param hypothenuse
     * @param A
     * @param B
     * @return un booleen qui dit si le triangle est rectangle ou non.
     */
    public static boolean verifie(double hypothenuse, double A, double B) {
        boolean carre = false;
       if(Math.pow(A,2) + Math.pow(B,2) == Math.pow(hypothenuse,2)){
           carre = true;
       }
       return carre;
    }

    
    //*******************************************
    
    /*
     * Vous devez créer un programme correspondant au pseudocode suivant:
     *
     * Lire 9 chiffres (un chiffre c'est 0 à 9)
     *    indice: utiliser un tableau
     *    Note: vous n'avez pas besoin de valider que c'est de chiffres de 0 à 9. Les tests utiliseront 0 à 9
     *
     *
     * Multiplier les positions paires par 2 (le premier est une position impaire)
     *
     *      Si le résultat de la multiplication par 2 est plus grand que 9, alors additionner les deux chiffres
     *          (ex: 8 * 2 = 16 -> 1+ 6 = 7)
     *          indice: c'est une belle place pour utiliser un "switch" (il y a aussi d'autres façons de le faire)
     *
     * Ne changez pas les positions impaires.
     *
     * Additionner les 9 chiffres résultants
     *
     * Si le résultat est divisible par 10, alors afficher "valide", sinon afficher "invalide"
     *
     * Vous vous demandez à quoi sert cet algorithme?
     *      Il sert a valider si un NAS (Numéros d'Assurance Sociale) est valide.
     *      Essayez le avec votre NAS si vous en avez un.
     *
     * Exemple:
     * Les numéros suivants sont valides:
     * 0 4 4 0 9 6 8 5 7
     * 0 4 6 4 5 4 2 8 6
     * 1 2 3 4 5 6 7 8 2
     *
     * Les numéros suivants sont invalides:
     * 0 4 4 0 9 6 8 5 6
     * 0 4 6 4 5 4 2 8 7
     */

    public void pseudo2() {

        //multiplie les chiffres position paire par 2 et addittionne les 2 chiffres des résultats plus grand que 10

        int[] serie = remplirTableau();

        for (int i = 1;i < serie.length;i+=2){
            serie[i] = serie[i]*2;
            if (serie[i] >= 10){
                serie[i] = 1 + (serie[i] - 10);
            }
        }
        //additionne la serie de chiffres
        int total = 0;
        for (int i = 0;i < serie.length;i++){
            total += serie[i];
        }
        //verifie si le total est un multiple de 10
        if (total % 10 == 0){
            System.out.println("valide");
        }else{
            System.out.println("invalide");
        }
    }



    private int[] remplirTableau() {
        Scanner s = new Scanner(System.in);

        int[] tableau = new int[9];
        int element;
        for (int i = 0; i < tableau.length; i++) {
            element = s.nextInt();
            tableau[i] = element;
        }
        return tableau;
    }

}