package ca.cegepdrummond;

import java.util.Scanner;// importe la librairie scanner

public class Main {

    public static void main(String[] args) {
        Serie1_Introduction serie1 = new Serie1_Introduction();
        Serie2_Variables serie2 = new Serie2_Variables();
        Serie3_OperateursEtConditions serie3 = new Serie3_OperateursEtConditions();
        Serie4_EntreesClavier serie4 = new Serie4_EntreesClavier();
        Serie5_OperateursBooleens serie5 = new Serie5_OperateursBooleens();
        Serie6_String serie6 = new Serie6_String();
        Serie7_Boucles serie7 = new Serie7_Boucles();
        Serie10_Math serie10 = new Serie10_Math();
        Serie11_Fonctions serie11 = new Serie11_Fonctions();
        Serie13_Tableaux serie13 = new Serie13_Tableaux();
        Serie14_Pseudocode serie14 = new Serie14_Pseudocode();
        Serie15_Revision serie15 = new Serie15_Revision();


        String choix = "";
        String choixPrecedent = "";
        Scanner scanner = new Scanner(System.in);
        while (!choix.equals("0")) {
            choixPrecedent = choix;
            System.out.println("Quel exercice désirez-vous exécuter? (0 pour terminer) ");
            choix = scanner.nextLine();
            if (choix.equals(".")) {
                choix = choixPrecedent;
            }
            switch(choix.trim()) {
                case "0": System.out.println("bye"); break;
                case "1a": serie1.HelloWorld();break;
                case "1b": serie1.print1();break;
                case "1c": serie1.print2();break;
                case "1d": serie1.print3();break;

                case "2a": serie2.variable1();break;
                case "2b": serie2.variable2();break;
                case "2c": serie2.variable3();break;
                case "2d": serie2.variable4();break;

                case "3a": serie3.operateur1();break;
                case "3b": serie3.operateur2();break;
                case "3c": serie3.operateur3();break;
                case "3d": serie3.operateur4();break;
                case "3e": serie3.operateur5();break;
                case "3f": serie3.operateur6();break;
                case "3g": serie3.condition1();break;
                case "3h": serie3.condition2();break;
                case "3i":serie3.condition3();break;

                case "4a": serie4.clavier1();break;
                case "4b": serie4.clavier2();break;
                case "4c": serie4.clavier3();break;
                case "4d": serie4.clavier4(); break;
                case "4e": serie4.clavier5(); break;
                case "4f": serie4.clavier6(); break;

                case "5a": serie5.bool1(); break;
                case "5b": serie5.bool2(); break;
                case "5c": serie5.bool3(); break;
                case "5d": serie5.bool4(); break;
                case "5e": serie5.bool5(); break;
                case "5f": serie5.bool6(); break;
                case "5g": serie5.bool7(); break;
                case "5h": serie5.boolEtClavier1(); break;
                case "5i": serie5.boolEtClavier2(); break;
                case "5j": serie5.boolEtClavier3();break;
                case "5k": serie5.boolEtClavier4(); break;
                case "5l": serie5.boolEtClavier5(); break;

                case "6a": serie6.string1(); break;
                case "6b": serie6.string2(); break;
                case "6c": serie6.string3(); break;
                case "6d": serie6.string4(); break;
                case "6e": serie6.string5(); break;
                case "6f": serie6.string6(); break;

                case "7a": serie7.while1(); break;
                case "7b": serie7.while2(); break;
                case "7c": serie7.while3(); break;
                case "7d": serie7.dowhile1(); break;
                case "7e": serie7.dowhile2(); break;
                case "7f": serie7.dowhile3(); break;
                case "7g": serie7.for1(); break;
                case "7h": serie7.for2(); break;
                case "7i": serie7.for3(); break;
                case "7j": serie7.boucle1(); break;
                case "7k": serie7.boucle2(); break;
                case "7l": serie7.boucle3(); break;
                case "7m": serie7.boucle4(); break;
                case "7n": serie7.boucle5(); break;

                case "10a": serie10.math1(); break;
                case "10b": serie10.math2(); break;

                case "11a": serie11.fonction1(); break;
                case "11b": serie11.fonction2(); break;
                case "11c": serie11.fonction3(); break;
                case "11d": serie11.fonction4(); break;
                case "11e": serie11.fonction5(); break;
                case "11f": serie11.fonction6(); break;
                case "11g": serie11.fonction7(); break;
                case "11h": serie11.fonction8(); break;
                case "11i": serie11.fonction9(); break;

                case "13a": serie13.tableau1(); break;
                case "13b": serie13.tableau2(); break;
                case "13c": serie13.tableau3(); break;
                case "13d": serie13.tableau4(); break;
                case "13e": serie13.tableau5(); break;
                case "13f": serie13.tictactoe(); break;

                case "14a": serie14.pseudo1(); break;
                case "14b": serie14.pseudo2(); break;

                case "15a": serie15.revision1(); break;
                case "15b": serie15.revision2(); break;


                default:
                    System.out.println("choix invalide");
                    break;

            }

        }
    }
}
